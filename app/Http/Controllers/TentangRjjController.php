<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TentangRjjController extends Controller
{
    public function index()
    {
        return view('pages.tentangRjj', [
            "title" => "Tentang RJJ"
        ]);
    }
}
