<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UpdateMemberController extends Controller
{
    public function index()
    {
        return view('pages.updateMember', [
            "title" => "Update Member"
        ]);
    }
}
