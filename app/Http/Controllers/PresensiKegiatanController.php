<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PresensiKegiatanController extends Controller
{
    public function index()
    {
        return view('pages.presensiKegiatan', [
            "title" => "Presensi Kegiatan",
            "group" => "presensi"
        ]);
    }
}
