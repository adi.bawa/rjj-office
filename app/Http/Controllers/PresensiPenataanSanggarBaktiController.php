<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PresensiPenataanSanggarBaktiController extends Controller
{
    public function index()
    {
        return view('pages.presensiPenataanSanggarBakti', [
            "title" => "Presensi Penataan Sanggar Bakti",
            "group" => "presensi"
        ]);
    }
}
