<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PresensiRapatController extends Controller
{
    public function index()
    {
        return view('pages.presensiRapat', [
            "title" => "Presensi Rapat",
            "group" => "presensi"
        ]);
    }
}
