<?php

use App\Http\Controllers\BerandaController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\DokumentasiKegiatanController;
use App\Http\Controllers\PresensiKegiatanController;
use App\Http\Controllers\PresensiPenataanSanggarBaktiController;
use App\Http\Controllers\PresensiRapatController;
use App\Http\Controllers\TentangRjjController;
use App\Http\Controllers\UpdateMemberController;
use Illuminate\Support\Facades\Route;

Route::get('/', [BerandaController::class, 'index']);

Route::get('/contact', [ContactController::class, 'index']);

Route::get('/dokumentasiKegiatan', [DokumentasiKegiatanController::class, 'index']);

Route::get('/presensiKegiatan', [PresensiKegiatanController::class, 'index']);

Route::get('/presensiPenataanSanggarBakti', [PresensiPenataanSanggarBaktiController::class, 'index']);

Route::get('/presensiRapat', [PresensiRapatController::class, 'index']);

Route::get('/tentangRjj', [TentangRjjController::class, 'index']);

Route::get('/updateMember', [UpdateMemberController::class, 'index']);