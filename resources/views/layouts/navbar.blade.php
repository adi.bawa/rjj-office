<nav class="navbar navbar-expand-lg navbar-light bg-info">
    <div class="container-fluid">
      <a class="navbar-brand" href="/"> <img src="img/rjj.png" alt="" width="35px"> RJJ Office</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link {{ ($title === "Tentang RJJ") ? 'active' : ''}}" href="/tentangRjj">Tentang RJJ</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ ($title === "Dokumentasi Kegiatan") ? 'active' : ''}}" href="/dokumentasiKegiatan">Dokumentasi Kegiatan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ ($title === "Update Member") ? 'active' : ''}}" href="/updateMember">Update Member</a>
          </li>
          <li class="nav-item dropdown ">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Presensi
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <li><a class="dropdown-item" href="/presensiRapat">Rapat</a></li>
              <li><a class="dropdown-item" href="/presensiKegiatan">Kegiatan</a></li>
              <li><a class="dropdown-item" href="/presensiPenataanSanggarBakti">Penataan Sanggar Bakti</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>  
</nav>